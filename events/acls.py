from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


# REQUESTS FROM PEXELS API
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]


# REQUESTS FROM OPEN WEATHER API
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the lat and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
def get_weather_data(city, state):
    url_one = "http://api.openweathermap.org/geo/1.0/direct"
    header = {
        "Authorization": OPEN_WEATHER_API_KEY,
    }
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url_one, params=params, headers=header)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url_two = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url_two, params=params)
    content = response.json()

    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }
